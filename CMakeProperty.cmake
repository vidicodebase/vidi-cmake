# ┌──────────────────────────────────────────────────────────────────┐
# │ ViDi Codebase                                                    │
# │  Copyright(c) 2018 - 2020 Qi Wu                                  │
# │  University of California, Davis                                 │
# │  MIT Licensed                                                    │
# └──────────────────────────────────────────────────────────────────┘
include_guard(GLOBAL)
function(vidi_cmake_loaded)
  message(FATAL_ERROR "this function should not be called")
endfunction()

# ┌────────────────────────────────────────────────────────────────────┐
# │ API: generate functions                                            │
# └────────────────────────────────────────────────────────────────────┘
function(vidi_generate_target_properties)

  # process arguments
  set(options)
  set(oneArgs PREFIX)
  set(mulArgs)
  cmake_parse_arguments(in "${options}" "${oneArgs}" "${mulArgs}" ${ARGN})

  # customized behaviors
  if(in_PREFIX)
    set(filename "${CMAKE_BINARY_DIR}/_GenTargetProp_${in_PREFIX}.cmake")
    set(fun vidi_${in_PREFIX}_target_property)
    set(var __vidi_${in_PREFIX}__)
  else()
    set(filename "${CMAKE_BINARY_DIR}/_GenTargetProp.cmake")
    set(fun vidi_target_property)
    set(var __vidi__)
  endif()

  # generate
  file(WRITE ${filename}
    "# ┌────────────────────────────────────────────────────────────────────┐\n"
    "# │ ViDi Codebase                                                      │\n"
    "# │  Copyright(c) 2018 - 2020 Qi Wu                                    │\n"
    "# │  University of California, Davis                                   │\n"
    "# │  MIT Licensed                                                      │\n"
    "# │                                                                    │\n"
    "# │  Automatically generated file, DO NOT modify!                      │\n"
    "# │                                                                    │\n"
    "# └────────────────────────────────────────────────────────────────────┘\n"
    "# ┌──────────────────────────────────────────────────────────────────── \n"
    "# │ generic version                                                     \n"
    "# └──────────────────────────────────────────────────────────────────── \n"
    "function(${fun}_set in_target in_prop)                                  \n"
    "  set_property(TARGET \${in_target} PROPERTY ${var}\${in_prop} \${ARGN})\n"
    "endfunction()                                                           \n"
    "function(${fun}_get in_target in_prop in_output)                        \n"
    "  get_property(out TARGET \${in_target} PROPERTY ${var}\${in_prop})     \n"
    "  set(\${in_output} \${out} PARENT_SCOPE)                               \n"
    "endfunction()                                                           \n"
    "\n")
  foreach(prop ${in_UNPARSED_ARGUMENTS})
    file(APPEND ${filename}
      "# ┌──────────────────────────────────────────────────────────────────── \n"
      "# │ target property ${prop}                                             \n"
      "# └──────────────────────────────────────────────────────────────────── \n"
      "function(${fun}_set_${prop} in_target)                                  \n"
      "  set_property(TARGET \${in_target} PROPERTY ${var}${prop} \${ARGN})    \n"
      "endfunction()                                                           \n"
      "function(${fun}_get_${prop} in_target in_output)                        \n"
      "  get_property(out TARGET \${in_target} PROPERTY ${var}${prop})         \n"
      "  set(\${in_output} \${out} PARENT_SCOPE)                               \n"
      "endfunction()                                                           \n"
      "\n")
  endforeach()
  include(${filename})

endfunction()

function(vidi_generate_global_properties)

  # process arguments
  set(options)
  set(oneArgs PREFIX)
  set(mulArgs)
  cmake_parse_arguments(in "${options}" "${oneArgs}" "${mulArgs}" ${ARGN})

  # customized behaviors
  if(in_PREFIX)
    set(filename "${CMAKE_BINARY_DIR}/_GenGlobalProp_${in_PREFIX}.cmake")
    set(fun vidi_${in_PREFIX}_property)
    set(var __vidi_${in_PREFIX}__)
  else()
    set(filename "${CMAKE_BINARY_DIR}/_GenGlobalProp.cmake")
    set(fun vidi_global_property)
    set(var __vidi__)
  endif()

  # generate
  file(WRITE ${filename}
    "# ┌────────────────────────────────────────────────────────────────────┐\n"
    "# │ ViDi Codebase                                                      │\n"
    "# │  Copyright(c) 2018 - 2020 Qi Wu                                    │\n"
    "# │  University of California, Davis                                   │\n"
    "# │  MIT Licensed                                                      │\n"
    "# │                                                                    │\n"
    "# │  Automatically generated file, DO NOT modify!                      │\n"
    "# │                                                                    │\n"
    "# └────────────────────────────────────────────────────────────────────┘\n"
    "# ┌──────────────────────────────────────────────────────────────────── \n"
    "# │ generic version                                                     \n"
    "# └──────────────────────────────────────────────────────────────────── \n"
    "function(${fun}_set in_prop)                                            \n"
    "  set_property(GLOBAL PROPERTY ${var}\${in_prop} \${ARGN})              \n"
    "  #message(STATUS \"\${in_prop} \${ARGN}\")                             \n"
    "endfunction()                                                           \n"
    "function(${fun}_get in_prop in_output)                                  \n"
    "  get_property(out GLOBAL PROPERTY ${var}\${in_prop})                   \n"
    "  set(\${in_output} \${out} PARENT_SCOPE)                               \n"
    "endfunction()                                                           \n"
    "\n")
  foreach(prop ${in_UNPARSED_ARGUMENTS})
    file(APPEND ${filename}
      "# ┌──────────────────────────────────────────────────────────────────── \n"
      "# │ global property ${prop}                                             \n"
      "# └──────────────────────────────────────────────────────────────────── \n"
      "function(${fun}_set_${prop})                                            \n"
      "  set_property(GLOBAL PROPERTY ${var}${prop} \${ARGN})                  \n"
      "endfunction()                                                           \n"
      "function(${fun}_get_${prop} in_output)                                  \n"
      "  get_property(out GLOBAL PROPERTY ${var}${prop})                       \n"
      "  set(\${in_output} \${out} PARENT_SCOPE)                               \n"
      "endfunction()                                                           \n"
      "\n")
  endforeach()
  include(${filename})

endfunction()

# ┌────────────────────────────────────────────────────────────────────┐
# │ API: execute function generation                                   │
# └────────────────────────────────────────────────────────────────────┘
vidi_generate_target_properties(
  HEADER
  EXPORT
  INSTALL
  DLLEXPORT
  NAMESPACE
)

function(vidi_project_configure)

  # parse arguments
  set(options
    FORCE_WARN_AS_ERROR
    INSTALL_DEV_HEADERS)
  set(oneArgs
    LIB_OUTPUT_PATH
    EXE_OUTPUT_PATH
    CXX_COMPILE_STD
    CXX_COMPILE_STD_FLAG
    INSTALL_LIBDIR
    INSTALL_BINDIR
    INSTALL_INCDIR
    3RDPARTY_ROOT)
  set(mulArgs)
  cmake_parse_arguments(in "${options}" "${oneArgs}" "${mulArgs}" ${ARGN})
  if(in_UNPARSED_ARGUMENTS)
    foreach(arg ${in_UNPARSED_ARGUMENTS})
      message(WARNING "Unparsed argument: ${arg}")
    endforeach()
  endif()
  vidi_generate_global_properties(${options} ${oneArgs} ${mulArgs})

  # make sure this function only got called once
  get_property(first GLOBAL PROPERTY __first_vidi_project_configure)
  if(first)
    message(WARNING "function \"vidi_project_configure\" should not be called for multiple times")
  endif()
  set_property(GLOBAL PROPERTY __first_vidi_project_configure TRUE)

  # treat options specially
  if(in_FORCE_WARN_AS_ERROR)
    vidi_global_property_set(FORCE_WARN_AS_ERROR TRUE)
  endif()
  if(in_INSTALL_DEV_HEADERS)
    vidi_global_property_set(INSTALL_DEV_HEADERS TRUE)
  endif()

  # save them as global properties
  foreach(var ${oneArgs})
    if(in_${var})
      #message(STATUS "${var}: ${in_${var}}")
      vidi_global_property_set(${var} ${in_${var}})
    endif()
  endforeach()

endfunction()

macro(vidi_require_onevar var)
  if(NOT DEFINED in_${var})
    get_property(exists GLOBAL PROPERTY __vidi__${var} SET)
    if(NOT exists)
      message(FATAL_ERROR "required parameter ${var} not given.")
    else()
      vidi_global_property_get(${var} in_${var})
    endif()
  endif()
endmacro()

macro(vidi_require_option var)
  if(NOT in_${var})
    vidi_global_property_get(${var} in_${var})
  endif()
endmacro()
