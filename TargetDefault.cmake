# ┌──────────────────────────────────────────────────────────────────┐
# │ ViDi Codebase                                                    │
# │  Copyright(c) 2018 - 2020 Qi Wu                                  │
# │  University of California, Davis                                 │
# │  MIT Licensed                                                    │
# └──────────────────────────────────────────────────────────────────┘
include_guard(GLOBAL)

# ┌────────────────────────────────────────────────────────────────────┐
# │ API: In this step, all the properties of the target will be set,   │
# |      including include paths, installed files, exported targets.   |
# |      Call this function to every target once ...                   |
# └────────────────────────────────────────────────────────────────────┘
include(${CMAKE_CURRENT_LIST_DIR}/CMakeDoxygen.cmake)

function(vidi_target_finalize in_target)

  # parse arguments
  set(options
    FORCE_WARN_AS_ERROR
    INSTALL_DEV_HEADERS)
  set(oneArgs
    LIB_OUTPUT_PATH
    EXE_OUTPUT_PATH
    CXX_COMPILE_STD
    CXX_COMPILE_STD_FLAG
    INSTALL_LIBDIR
    INSTALL_BINDIR
    INSTALL_INCDIR)
  set(mulArgs)
  cmake_parse_arguments(in "${options}" "${oneArgs}" "${mulArgs}" ${ARGN})
  if(in_UNPARSED_ARGUMENTS)
    foreach(arg ${in_UNPARSED_ARGUMENTS})
      message(WARNING "Unparsed argument: ${arg}")
    endforeach()
  endif()

  # dependency
  vidi_require_option(FORCE_WARN_AS_ERROR)
  vidi_require_option(INSTALL_DEV_HEADERS)
  vidi_require_onevar(LIB_OUTPUT_PATH)
  vidi_require_onevar(EXE_OUTPUT_PATH)
  vidi_require_onevar(CXX_COMPILE_STD)
  vidi_require_onevar(CXX_COMPILE_STD_FLAG)
  vidi_require_onevar(INSTALL_LIBDIR)
  vidi_require_onevar(INSTALL_BINDIR)
  vidi_require_onevar(INSTALL_INCDIR)

  # setup install directory
  target_include_directories(${in_target} PUBLIC
    $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>)
  target_include_directories(${in_target} PUBLIC
    $<BUILD_INTERFACE:${CMAKE_CURRENT_BINARY_DIR}>)

  # c++ standard
  if(in_CXX_COMPILE_STD AND in_CXX_COMPILE_STD_FLAG)
    # message(STATUS "standard(${in_target} ${in_CXX_COMPILE_STD} ${in_CXX_COMPILE_STD_FLAG})")
    vidi_target_standard(
      ${in_target}
      ${in_CXX_COMPILE_STD}
      ${in_CXX_COMPILE_STD_FLAG}
    )
  endif()

  # other configurations
  set_target_properties(${in_target} PROPERTIES
    ARCHIVE_OUTPUT_DIRECTORY ${in_LIB_OUTPUT_PATH}
    LIBRARY_OUTPUT_DIRECTORY ${in_LIB_OUTPUT_PATH}
    RUNTIME_OUTPUT_DIRECTORY ${in_EXE_OUTPUT_PATH})

  # treat warning as error
  if(in_FORCE_WARN_AS_ERROR)
    target_compile_options(${in_target} PRIVATE
      $<$<AND:$<COMPILE_LANGUAGE:CXX>,$<CXX_COMPILER_ID:MSVC>>:/WX>
      $<$<AND:$<COMPILE_LANGUAGE:CXX>,$<CXX_COMPILER_ID:GNU>>:-Werror>)
  endif()

  # some extra compiler options
  target_compile_options(${in_target} PRIVATE
    $<$<AND:$<COMPILE_LANGUAGE:CXX>,$<CXX_COMPILER_ID:PGI>>:--diag_suppress=177>  # unused function
    $<$<AND:$<COMPILE_LANGUAGE:CXX>,$<CXX_COMPILER_ID:PGI>>:--diag_suppress=445>  # unused template
    $<$<AND:$<COMPILE_LANGUAGE:CXX>,$<CXX_COMPILER_ID:PGI>>:--diag_suppress=550>  # unused variable
    $<$<AND:$<COMPILE_LANGUAGE:CXX>,$<CXX_COMPILER_ID:PGI>>:--diag_suppress=1215> # deprecated
    $<$<AND:$<COMPILE_LANGUAGE:CXX>,$<CXX_COMPILER_ID:PGI>>:--display_error_number>)

  # headers
  vidi_target_property_get_header(${in_target} in_HEADER)

  # setup export target
  vidi_target_property_get_export(${in_target} in_EXPORT)
  if(in_EXPORT)
    set(export_target ${in_EXPORT}-config)
    set(args_export EXPORT ${export_target})
  endif()

  # install the target (object libraries are handled differently)
  get_property(in_TYPE TARGET ${in_target} PROPERTY TYPE)
  if(NOT (in_TYPE STREQUAL "OBJECT_LIBRARY"))
    install(TARGETS ${in_target} ${args_export}
      ARCHIVE DESTINATION ${in_INSTALL_LIBDIR}
      LIBRARY DESTINATION ${in_INSTALL_LIBDIR}
      RUNTIME DESTINATION ${in_INSTALL_BINDIR})
  endif()

  # export targets
  if(in_INSTALL_DEV_HEADERS AND in_EXPORT)

    # add namespace to the export target
    vidi_target_property_get_namespace(${in_target} namespace)
    if(namespace)
      set(args_namespace NAMESPACE ${namespace}::)
    endif()

    # this makes the project importable from the install directory
    # Put config file in per-project dir (name MUST match), can also
    # just go into 'cmake'.
    install(EXPORT ${export_target} ${args_namespace}
      DESTINATION ${in_INSTALL_LIBDIR}/cmake)

    # # this makes the project importable from the build directory
    # export(TARGETS ${in_target} ${args_namespace}
    #   FILE ${CMAKE_BINARY_DIR}/cmake/${export_target}.cmake)

    # # register configure files
    # get_property(var GLOBAL PROPERTY VIDI_INSTALL_TARGET)
    # set_property(GLOBAL PROPERTY VIDI_INSTALL_TARGET ${var} ${export_target})

  endif()

  # install files
  vidi_target_property_get_install(${in_target} in_INSTALL)
  if(in_INSTALL_DEV_HEADERS AND in_INSTALL)

    # setup install directory
    target_include_directories(${in_target} PUBLIC
      $<INSTALL_INTERFACE:${in_INSTALL_INCDIR}/${in_INSTALL}>)

    # create directories
    install(DIRECTORY DESTINATION ${in_INSTALL_INCDIR}/${in_INSTALL})

    # Because object libraries may contain extra include interface, we need to fix them.
    # However this task is not that complicated because we know the relative path between
    # the linked library and object libraries' root, and we always want to include the root
    # directory only. So we can directly compute the correct path here.
    #get_property(obj_RELPATH TARGET ${in_target} PROPERTY VIDI_TARGET_OBJECT_RELPATH)
    #foreach(r ${obj_RELPATH})
    #  target_include_directories(${in_target} PUBLIC
    #    $<INSTALL_INTERFACE:${in_INSTALL_INCDIR}/${in_INSTALL}/${r}>)
    #endforeach()
    #foreach(r ${obj_RELPATH})
    #  install(DIRECTORY DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/${in_INSTALL}/${r})
    #endforeach()

    # install headers
    foreach(f ${in_HEADER})
      get_filename_component(d ${f} DIRECTORY)
      install(FILES ${f} DESTINATION
        ${in_INSTALL_INCDIR}/${in_INSTALL}/${d})
    endforeach()

    # install dll export headers for windows
    vidi_target_property_get_dllexport(${in_target} in_DLLEXPORT)
    if(in_DLLEXPORT)
      install(FILES ${CMAKE_CURRENT_BINARY_DIR}/${in_DLLEXPORT}_export.h
        DESTINATION ${in_INSTALL_INCDIR}/${in_INSTALL})
    endif()

  endif()

  # register doxygen files
  vidi_doc_add(${in_SOURCES} ${in_HEADERS})

endfunction()
