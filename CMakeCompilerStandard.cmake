# ┌──────────────────────────────────────────────────────────────────┐
# │ ViDi Codebase                                                    │
# │  Copyright(c) 2018 - 2020 Qi Wu                                  │
# │  University of California, Davis                                 │
# │  MIT Licensed                                                    │
# └──────────────────────────────────────────────────────────────────┘
include_guard(GLOBAL)

# ┌──────────────────────────────────────────────────────────────────┐
# |  API: we use this function to test if one particular C++         |
# |  standard is available to use.                                   |
# └──────────────────────────────────────────────────────────────────┘
function(check_one_cxx_standard
  out_avail # whether the standard is available
  out_flags # computed compiler flag
  in_std    # the requested cxx standard
  )

  # different C++ compiler
  if(CMAKE_CXX_COMPILER_ID STREQUAL "MSVC")
    check_cxx_compiler_flag("/std:c++${in_std}" has_stdcxx_${in_std}_msvc)
    if(has_stdcxx_${in_std}_msvc)
      set(avail TRUE)
      set(flag "/std:c++${in_std}")
    endif()
  elseif(CMAKE_CXX_COMPILER_ID STREQUAL "PGI")
    check_cxx_compiler_flag("-std=c++${in_std} --gnu_extensions" has_stdcxx_${in_std}_pgi)
    if(has_stdcxx_${in_std}_pgi) 
      set(avail TRUE) 
      set(flag "-std=c++${in_std} --gnu_extensions")
    endif()
  else()
    # GNU style as the fallback style
    check_cxx_compiler_flag("-std=c++${in_std}" has_stdcxx_${in_std}_gnu)
    if(has_stdcxx_${in_std}_gnu)
      set(avail TRUE)
      set(flag "-std=c++${in_std}")
    endif()
  endif()
  # more compiler styles come here ...
  # ....

  # return values
  if(avail)
    set(${out_avail} TRUE PARENT_SCOPE)
    set(${out_flags} "${flag}" PARENT_SCOPE)
  else()
    set(${out_avail} FALSE PARENT_SCOPE)
    unset(${out_flags} PARENT_SCOPE)
  endif()

endfunction()

# ┌──────────────────────────────────────────────────────────────────┐
# │  this function allows to automatically search for the latest C++ │
# │  standard available on the machine                               │
# └──────────────────────────────────────────────────────────────────┘
function(check_max_cxx_standard
  out_std   # the maximum available standard
  out_flags # the computed compiler flag
  # ... a list of standard keywords to check
  )

  # check if any of the required standard is valid
  foreach(std ${ARGN})
    check_one_cxx_standard(has_cxx_${std} flags_cxx_${std} ${std})
    if(has_cxx_${std})
      # set the standard as a global option
      set(${out_std} "${std}" PARENT_SCOPE)
      set(${out_flags} "${flags_cxx_${std}}" PARENT_SCOPE)
      # early exit
      return()
    endif()
  endforeach()
  # we found nothing, thus throw an error
  message(FATAL_ERROR "No suitable C++ standard found")

endfunction()

# ┌──────────────────────────────────────────────────────────────────┐
# │  for each std available, a particular preprocessor variable      |
# │  will be defined as: VIDI_USE_CXX_STD_${STD}=1                   |
# └──────────────────────────────────────────────────────────────────┘
function(__vidicompilerstandard__set_target_definitions
  in_target # the target to be configured
  # ... list of requested definitions
  )
  # check if any of the required standard is valid
  foreach(std ${ARGN})
    target_compile_definitions(${in_target}
      PRIVATE
      VIDI_USE_CXX_STD_${std}=1)
  endforeach()
endfunction()

# ┌──────────────────────────────────────────────────────────────────┐
# │  API: set cxx standard to a target                               │
# └──────────────────────────────────────────────────────────────────┘
function(vidi_target_standard
  in_target # the target to be configured
  in_std    # the string containing the required standard
  in_flags  # the variable holding the corresponding compiler option
  )

  # enable the compile option
  string(REPLACE " " ";" in_flags ${in_flags})
  target_compile_options(${in_target} PRIVATE $<$<COMPILE_LANGUAGE:CXX>:${in_flags}>)

  # define macros for all lower standards
  if(in_std STREQUAL "latest")
    __vidicompilerstandard__set_target_definitions(
      ${in_target} 
      14 11)
  endif()
  if((in_std STREQUAL "20") OR (in_std STREQUAL "20"))
    __vidicompilerstandard__set_target_definitions(
      ${in_target}
      20 17 14 11)
  endif()
  if((in_std STREQUAL "17") OR (in_std STREQUAL "1z"))
    __vidicompilerstandard__set_target_definitions(
      ${in_target}
      17 14 11)
  endif()
  if((in_std STREQUAL "14") OR (in_std STREQUAL "1y"))
    __vidicompilerstandard__set_target_definitions(
      ${in_target}
      14 11)
  endif()
  if((in_std STREQUAL "11") OR (in_std STREQUAL "1x") OR (in_std STREQUAL "0x"))
    __vidicompilerstandard__set_target_definitions(
      ${in_target}
      11)
  endif()

endfunction()
