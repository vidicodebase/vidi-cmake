# ┌──────────────────────────────────────────────────────────────────┐
# │ ViDi Codebase                                                    │
# │  Copyright(c) 2018 - 2020 Qi Wu                                  │
# │  University of California, Davis                                 │
# │  MIT Licensed                                                    │
# └──────────────────────────────────────────────────────────────────┘
include_guard(GLOBAL)

function(vidi_cmake_option
  in_opt in_doc in_value)

  # parse arguments
  set(options ADVANCED FORCE)
  set(oneArgs)
  set(mulArgs VALUES)
  cmake_parse_arguments(in "${options}" "${oneArgs}" "${mulArgs}" ${ARGN})
  if(in_UNPARSED_ARGUMENTS)
    foreach(arg ${in_UNPARSED_ARGUMENTS})
      message(WARNING "Unparsed argument: ${arg}")
    endforeach()
  endif()

  # define the option properly. if multiple values are provided, define the variable
  # as a cache variable
  if(in_VALUES) # if ``VALUES`` is not provided, we consider it as a regular option
    if(in_FORCE)
      set(argv FORCE)
    endif()
    set(${in_opt} ${in_value} CACHE STRING "${in_doc}" ${argv})
    set_property(CACHE ${in_opt} PROPERTY STRINGS ${in_VALUES})
  else()
    option(${in_opt} "${in_doc}" ${in_value})
    if(in_FORCE)
      set(${in_opt} ${in_value} CACHE STRING "${in_doc}" ${argv} FORCE)
    endif()
  endif()

  # if we should hide the variable by default
  if(in_ADVANCED)
    mark_as_advanced(${in_opt})
  endif()
endfunction()
