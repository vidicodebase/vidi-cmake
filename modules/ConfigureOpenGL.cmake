# ┌──────────────────────────────────────────────────────────────────┐
# | Find OpenGL from System                                          |
# └──────────────────────────────────────────────────────────────────┘
include_guard(GLOBAL)
include(${CMAKE_CURRENT_LIST_DIR}/../CMakeProperty.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/../TargetLinkage.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/../TargetDefine.cmake)

vidi_generate_global_properties(PREFIX opengl
  OpenGL_AVAIL
  GL_AVAIL
  GLU_AVAIL
  GLX_AVAIL
  EGL_AVAIL
  OPENGL_LOADING_LIBRARY
  DISABLE_HEADLESS_MODE)

# ┌──────────────────────────────────────────────────────────────────┐
# | The marco to automatically link opengl and its other             |
# | dependencies  to the target                                      |
# └──────────────────────────────────────────────────────────────────┘
function(vidi_find_opengl)
  #
  # parse arguments
  set(options DISABLE_HEADLESS_MODE)
  set(oneArgs OPENGL_LOADING_LIBRARY)
  set(mulArgs)
  cmake_parse_arguments(in "${options}" "${oneArgs}" "${mulArgs}" ${ARGN})
  if(in_UNPARSED_ARGUMENTS)
    foreach(arg ${in_UNPARSED_ARGUMENTS})
      message(WARNING "Unparsed argument: ${arg}")
    endforeach()
  endif()
  #
  #  find OpenGL
  set(OpenGL_GL_PREFERENCE GLVND)
  find_package(OpenGL REQUIRED)
  #
  # setup global flags
  #
  if(TARGET OpenGL::OpenGL)
    vidi_opengl_property_set_opengl_avail(TRUE)
    message(STATUS "Found OpenGL::OpenGL")
  else()
    vidi_opengl_property_set_gl_avail(TRUE)
    message(STATUS "Found OpenGL::GL")
  endif()
  if(TARGET OpenGL::GLU)
    vidi_opengl_property_set_glu_avail(TRUE)
    message(STATUS "Found OpenGL::GLU")
  endif()
  if(TARGET OpenGL::GLX)
    vidi_opengl_property_set_glx_avail(TRUE)
    message(STATUS "Found OpenGL::GLX")
  endif()
  if(TARGET OpenGL::EGL AND (NOT in_DISABLE_HEADLESS_MODE))
    vidi_opengl_property_set_egl_avail(TRUE)
    message(STATUS "Found OpenGL::EGL")
  endif()
  #
  # headless mode
  if(in_DISABLE_HEADLESS_MODE)
    vidi_opengl_property_set_disable_headless_mode(TRUE)
  endif()
  if(NOT in_OPENGL_LOADING_LIBRARY)
    set(in_OPENGL_LOADING_LIBRARY GLAD)
  endif()
  vidi_opengl_property_set_opengl_loading_library(${in_OPENGL_LOADING_LIBRARY})
  #
  # find loading libraries
  #
  if(in_OPENGL_LOADING_LIBRARY STREQUAL GLEW)
    find_package(GLEW REQUIRED)
  elseif(in_OPENGL_LOADING_LIBRARY STREQUAL GLAD)
    if(NOT TARGET GLAD::GLAD)
      if(NOT TARGET glad::glad)
        find_package(glad REQUIRED)
	set_target_properties(glad::glad PROPERTIES IMPORTED_GLOBAL TRUE)
      endif()
      add_library(GLAD::GLAD ALIAS glad::glad)
    endif()
  else()
    message(FATAL_ERROR
      "Unknown OpenGL loading library ${in_OPENGL_LOADING_LIBRARY}")
  endif()
endfunction()

# ┌──────────────────────────────────────────────────────────────────┐
# | The marco to automatically link opengl and its other             |
# | dependencies to the target                                       |
# └──────────────────────────────────────────────────────────────────┘
function(vidi_target_link_opengl in_target)

  # parse arguments
  set(options)
  set(oneArgs FORCE)
  set(mulArgs)
  cmake_parse_arguments(in "${options}" "${oneArgs}" "${mulArgs}" ${ARGN})
  if(in_UNPARSED_ARGUMENTS)
    foreach(arg ${in_UNPARSED_ARGUMENTS})
      message(WARNING "Unparsed argument: ${arg}")
    endforeach()
  endif()

  # check input parameters
  if(in_FORCE STREQUAL EGL)
    set(use_egl TRUE)
  elseif(in_FORCE STREQUAL GLX)
    set(use_glx TRUE)
  else()
    set(no_context TRUE)
  endif()
  set(gl_defs VIDI_USE_OPENGL=1)
  set(gl_libs)

  # -- OpenGL core libraries
  if(TARGET OpenGL::OpenGL)
    list(APPEND gl_libs OpenGL::OpenGL)
  else()
    list(APPEND gl_libs OpenGL::GL)
  endif()
  if(TARGET OpenGL::GLU)
    list(APPEND gl_libs OpenGL::GLU)
  endif()

  # -- OpenGL context libraries
  get_property(in_DISABLE_HEADLESS_MODE GLOBAL 
  PROPERTY
    __vidiconfigopengl__DISABLE_HEADLESS_MODE
  )
  if(use_egl)
    if((NOT in_DISABLE_HEADLESS_MODE) AND (TARGET OpenGL::EGL))
      list(APPEND gl_defs VIDI_USE_EGL=1)
      list(APPEND gl_libs OpenGL::EGL)
    else()
      if(in_DISABLE_HEADLESS_MODE AND TARGET OpenGL::EGL)
        message(WARNING "EGL is found but disabled")
      else()
        message(WARNING "EGL is asked but not found")
      endif()
      # try to automatically switch to GLX mode
      if(TARGET OpenGL::GLX)
        message(WARNING "Automatically switching to GLX since GLX is available")
        list(APPEND gl_defs VIDI_USE_GLX=1)
        list(APPEND gl_libs OpenGL::GLX)
      endif()
    endif()
  elseif(use_glx)
    if(TARGET OpenGL::GLX)
      list(APPEND gl_defs VIDI_USE_GLX=1)
      list(APPEND gl_libs OpenGL::GLX)
    endif()
  endif()

  # -- OpenGL loading libraries
  get_property(in_OPENGL_LOADING_LIBRARY GLOBAL 
  PROPERTY
    __vidiconfigopengl__OPENGL_LOADING_LIBRARY
  )
  if(in_OPENGL_LOADING_LIBRARY STREQUAL GLEW)
    if(TARGET GLEW::GLEW)
      list(APPEND gl_defs VIDI_USE_GLEW=1)
      list(APPEND gl_libs GLEW::GLEW)
    else()
      message(FATAL_ERROR "GLEW has been requested but not found!!")
    endif()
  else()
    if(TARGET GLAD::GLAD)
      list(APPEND gl_defs VIDI_USE_GLAD=1)
      list(APPEND gl_libs GLAD::GLAD)
    else()
      message(FATAL_ERROR "GLAD has been requested but not found!!")
    endif()
  endif()

  # check if the target is an executable or not
  get_target_property(type ${in_target} TYPE)
  if(type STREQUAL "OBJECT_LIBRARY")
    # message(FATAL_ERROR "Command does not work for object libraries.")
    vidi_target_linkage(${in_target} LINK_PRIVATE ${gl_libs})
    vidi_target_define(${in_target} DEFINE_PRIVATE ${gl_defs})
  elseif(type STREQUAL "EXECUTABLE")
    target_link_libraries(${in_target} ${gl_libs})
    target_compile_definitions(${in_target} PRIVATE ${gl_defs})
  else()
    # message(STATUS "link: ${in_target} ${in_LINK_OBJECT} ${gl_libs}\n")
    target_link_libraries(${in_target} PRIVATE ${gl_libs})
    target_compile_definitions(${in_target} PRIVATE ${gl_defs})
  endif()

endfunction()
