# ┌────────────────────────────────────────────────────────────────────┐
# │ Link an object library to the target                               │
# └────────────────────────────────────────────────────────────────────┘
include_guard(GLOBAL)
include(${CMAKE_CURRENT_LIST_DIR}/CMakeProperty.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/TargetLinkage.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/TargetDefine.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/TargetDefault.cmake)

# ┌────────────────────────────────────────────────────────────────────┐
# │ Helper function to create an object library                        │
# └────────────────────────────────────────────────────────────────────┘
function(vidi_add_object in_target)

  # Parse arguments
  set(options)
  set(oneArgs NAMESPACE)
  set(mulArgs OBJECTS SOURCES HEADERS
    LINK LINK_PUBLIC LINK_PRIVATE LINK_INTERFACE
    DEFINE_PUBLIC DEFINE_PRIVATE DEFINE_INTERFACE)
  cmake_parse_arguments(in "${options}" "${oneArgs}" "${mulArgs}" ${ARGN})
  if(in_UNPARSED_ARGUMENTS)
    foreach(arg ${in_UNPARSED_ARGUMENTS})
      message(WARNING "Unparsed argument: ${arg}")
    endforeach()
  endif()

  # create Library
  add_library(${in_target} OBJECT ${in_SOURCES} ${in_HEADERS})

  # add namespace if necessary
  if(in_NAMESPACE)
    add_library(${in_NAMESPACE}::${in_target} ALIAS ${in_target})
  endif()
  vidi_target_property_set_namespace(
    ${in_target} ${in_NAMESPACE}
  )

  # in our code base, an object library is always linked to a shared library
  target_compile_options(${in_target} PRIVATE $<$<NOT:$<CXX_COMPILER_ID:MSVC>>:-fPIC>)

  vidi_target_define(${in_target}
    DEFINE_PUBLIC ${in_DEFINE_PUBLIC}
    DEFINE_PRIVATE ${in_DEFINE_PRIVATE}
    DEFINE_INTERFACE ${in_DEFINE_INTERFACE})

  vidi_target_linkage(${in_target}
    LINK ${in_LINK}
    LINK_PUBLIC ${in_LINK_PUBLIC}
    LINK_PRIVATE ${in_LINK_PRIVATE}
    LINK_INTERFACE ${in_LINK_INTERFACE}
    LINK_OBJECT ${in_OBJECTS})

endfunction()
