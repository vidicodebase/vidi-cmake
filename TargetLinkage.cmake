# ┌──────────────────────────────────────────────────────────────────┐
# │ ViDi Codebase                                                    │
# │  Copyright(c) 2018 - 2020 Qi Wu                                  │
# │  University of California, Davis                                 │
# │  MIT Licensed                                                    │
# └──────────────────────────────────────────────────────────────────┘
include_guard(GLOBAL)

# ┌────────────────────────────────────────────────────────────────────┐
# │ Helper: to extract targets linked to provided objects. Those       |
# | targets will be used to populate corresponding input variables.    |
# └────────────────────────────────────────────────────────────────────┘
function(__viditargetlinkage__extract_object_linkage
  in_target
  out_LINK
  out_LINK_PUBLIC
  out_LINK_PRIVATE
  out_LINK_INTERFACE)

  unset(var_LINK)
  unset(var_LINK_PUBLIC)
  unset(var_LINK_PRIVATE)
  unset(var_LINK_INTERFACE)

  # Link object libraries
  foreach(obj ${ARGN})

    # Add dependencies
    add_dependencies(${in_target} ${obj})

    # Forward include & compile definitions
    # The same reason as below
    get_property(obj_INCLUDE_DIRECTORIES TARGET ${obj} PROPERTY INTERFACE_INCLUDE_DIRECTORIES)
    get_property(obj_COMPILE_DEFINITIONS TARGET ${obj} PROPERTY INTERFACE_COMPILE_DEFINITIONS)
    target_include_directories(${in_target} PUBLIC ${obj_INCLUDE_DIRECTORIES})
    target_compile_definitions(${in_target} PUBLIC ${obj_COMPILE_DEFINITIONS})

    # Receive forward library linkages
    get_property(obj_LINK           TARGET ${obj} PROPERTY __vidi__LINK)
    get_property(obj_LINK_PUBLIC    TARGET ${obj} PROPERTY __vidi__LINK_PUBLIC)
    get_property(obj_LINK_PRIVATE   TARGET ${obj} PROPERTY __vidi__LINK_PRIVATE)
    get_property(obj_LINK_INTERFACE TARGET ${obj} PROPERTY __vidi__LINK_INTERFACE)
    get_property(obj_LINK_OBJECT    TARGET ${obj} PROPERTY __vidi__LINK_OBJECT)

#    message(STATUS "obj: ${obj} ${obj_LINK_OBJECT}\n"
#      "\tPUBLIC    ${obj_LINK_PUBLIC}\n"
#      "\tPRIVATE   ${obj_LINK_PRIVATE}\n"
#      "\tINTERFACE ${obj_LINK_INTERFACE}\n")

    # Recursively link extra objects
    __viditargetlinkage__extract_object_linkage(${in_target}
      rec_LINK
      rec_LINK_PUBLIC
      rec_LINK_PRIVATE
      rec_LINK_INTERFACE
      ${obj_LINK_OBJECT})

    # Add linkages extracted from the object to the list
    list(APPEND var_LINK ${rec_LINK} ${obj_LINK})
    list(APPEND var_LINK_PUBLIC ${rec_LINK_PUBLIC} ${obj_LINK_PUBLIC})
    list(APPEND var_LINK_PRIVATE ${rec_LINK_PRIVATE} ${obj_LINK_PRIVATE})
    list(APPEND var_LINK_INTERFACE ${rec_LINK_INTERFACE} ${obj_LINK_INTERFACE})

  endforeach()

  set(${out_LINK} ${${out_LINK}} ${var_LINK} PARENT_SCOPE)
  set(${out_LINK_PUBLIC} ${${out_LINK_PUBLIC}} ${var_LINK_PUBLIC} PARENT_SCOPE)
  set(${out_LINK_PRIVATE} ${${out_LINK_PRIVATE}} ${var_LINK_PRIVATE} PARENT_SCOPE)
  set(${out_LINK_INTERFACE} ${${out_LINK_INTERFACE}} ${var_LINK_INTERFACE} PARENT_SCOPE)

#  message(STATUS "inner check [${in_target}] ${in_LINK} ${in_LINK_PUBLIC} ${in_LINK_PRIVATE} ${in_LINK_INTERFACE}")

endfunction()

# ┌────────────────────────────────────────────────────────────────────┐
# │ API: This step creates an object library target                    |
# └────────────────────────────────────────────────────────────────────┘
function(vidi_target_linkage
  in_target
  # ... keyword arguments
  #
  # optional
  #   LINK <...> link var plain linkage
  #   LINK_PUBLIC    <...> cmake public library
  #   LINK_PRIVATE   <...> cmake private library
  #   LINK_INTERFACE <...> cmake interface library
  #   LINK_OBJECT    <...>
  )

  # Parse arguments
  set(options)
  set(oneValueArgs)
  set(multiValueArgs
    LINK
    LINK_PUBLIC
    LINK_PRIVATE
    LINK_INTERFACE
    LINK_OBJECT)
  cmake_parse_arguments(in
    "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN})
  if(in_UNPARSED_ARGUMENTS)
    foreach(arg ${in_UNPARSED_ARGUMENTS})
      message(WARNING "Unparsed argument: ${arg}")
    endforeach()
  endif()

  # resolve object linkages at this step
  if(in_LINK_OBJECT)
  #  message(STATUS "sanity in    [${in_target}] ${in_LINK} ${in_LINK_PUBLIC} ${in_LINK_PRIVATE} ${in_LINK_INTERFACE}")
    __viditargetlinkage__extract_object_linkage(${in_target}
      in_LINK
      in_LINK_PUBLIC
      in_LINK_PRIVATE
      in_LINK_INTERFACE
      ${in_LINK_OBJECT})
  #  message(STATUS "sanity check [${in_target}] ${in_LINK} ${in_LINK_PUBLIC} ${in_LINK_PRIVATE} ${in_LINK_INTERFACE}")
  endif()
  if(in_LINK)
    list(REMOVE_DUPLICATES in_LINK)
  endif()
  if(in_LINK_PUBLIC)
    list(REMOVE_DUPLICATES in_LINK_PUBLIC)
  endif()
  if(in_LINK_PRIVATE)
    list(REMOVE_DUPLICATES in_LINK_PRIVATE)
  endif()
  if(in_LINK_INTERFACE)
    list(REMOVE_DUPLICATES in_LINK_INTERFACE)
  endif()
  if(in_LINK_OBJECT)
    list(REMOVE_DUPLICATES in_LINK_OBJECT)
  endif()
  
  # object libraries are handled differently
  get_property(in_TYPE TARGET ${in_target} PROPERTY TYPE)
  if(in_TYPE STREQUAL "OBJECT_LIBRARY")

    # Forward include & compile definitions
    # Because we automatically install targets, we cannot simply do
    #
    # -- target_include_directories(${in_target} PRIVATE $<TARGET_PROPERTY:${obj},INTERFACE_INCLUDE_DIRECTORIES>)
    # -- target_compile_definitions(${in_target} PRIVATE $<TARGET_PROPERTY:${obj},INTERFACE_COMPILE_DEFINITIONS>)
    #
    # Instead we have to be kind of explicit to avoid exposing object libraries publicly
    foreach(item ${in_LINK} ${in_LINK_PUBLIC} ${in_LINK_PRIVATE} ${in_LINK_INTERFACE} ${LINK_OBJECT})
      get_property(item_INCLUDE_DIRECTORIES TARGET ${item} PROPERTY INTERFACE_INCLUDE_DIRECTORIES)
      get_property(item_COMPILE_DEFINITIONS TARGET ${item} PROPERTY INTERFACE_COMPILE_DEFINITIONS)
      target_include_directories(${in_target} PUBLIC ${item_INCLUDE_DIRECTORIES})
      target_compile_definitions(${in_target} PUBLIC ${item_COMPILE_DEFINITIONS})
    endforeach()

    # We forward library linkages
    set_property(TARGET ${in_target} APPEND PROPERTY __vidi__LINK           ${in_LINK})
    set_property(TARGET ${in_target} APPEND PROPERTY __vidi__LINK_PUBLIC    ${in_LINK_PUBLIC})
    set_property(TARGET ${in_target} APPEND PROPERTY __vidi__LINK_PRIVATE   ${in_LINK_PRIVATE})
    set_property(TARGET ${in_target} APPEND PROPERTY __vidi__LINK_INTERFACE ${in_LINK_INTERFACE})
    set_property(TARGET ${in_target} APPEND PROPERTY __vidi__LINK_OBJECT    ${in_LINK_OBJECT})

    #get_property(obj_LINK           TARGET ${in_target} PROPERTY __vidi__LINK)
    #get_property(obj_LINK_PUBLIC    TARGET ${in_target} PROPERTY __vidi__LINK_PUBLIC)
    #get_property(obj_LINK_PRIVATE   TARGET ${in_target} PROPERTY __vidi__LINK_PRIVATE)
    #get_property(obj_LINK_INTERFACE TARGET ${in_target} PROPERTY __vidi__LINK_INTERFACE)
    #get_property(obj_LINK_OBJECT    TARGET ${in_target} PROPERTY __vidi__LINK_OBJECT)
    #message(STATUS "sanity check [${in_target}] ${obj_LINK} ${obj_LINK_PUBLIC} ${obj_LINK_PRIVATE} ${obj_LINK_INTERFACE}")

#    message(STATUS "link: ${in_target} ${in_LINK_OBJECT}\n"
#      "\tPUBLIC    ${in_LINK_PUBLIC}\n"
#      "\tPRIVATE   ${in_LINK_PRIVATE}\n"
#      "\tINTERFACE ${in_LINK_INTERFACE}\n")

  else()

    # Handle object libraries
    foreach(item ${LINK_OBJECT})
      get_property(item_INCLUDE_DIRECTORIES TARGET ${item} PROPERTY INTERFACE_INCLUDE_DIRECTORIES)
      get_property(item_COMPILE_DEFINITIONS TARGET ${item} PROPERTY INTERFACE_COMPILE_DEFINITIONS)
      target_include_directories(${in_target} PUBLIC ${item_INCLUDE_DIRECTORIES})
      target_compile_definitions(${in_target} PUBLIC ${item_COMPILE_DEFINITIONS})
    endforeach()

    # Link to other targets
    if(in_LINK)
      # If one library has been linked as plain, then all the
      # others have to be linked as plain
      target_link_libraries(${in_target}
        ${CMAKE_DL_LIBS} ${in_LINK} ${in_LINK_PUBLIC}
        ${in_LINK_PRIVATE} ${in_LINK_INTERFACE})
      # Generate a warning
      if(in_LINK_PUBLIC OR in_LINK_PRIVATE OR in_LINK_INTERFACE)
        message(WARN
          "If one library has been linked as plain, then "
          "all the others have to be linked as plain libraries")
      endif()
    else()
      # Link with keyword style
      # message(STATUS "link: ${in_target} ${in_LINK_OBJECT}\n"
      #                "\tPUBLIC    ${in_LINK_PUBLIC}\n"
      #                "\tPRIVATE   ${in_LINK_PRIVATE}\n"
      #                "\tINTERFACE ${in_LINK_INTERFACE}\n")
      target_link_libraries(${in_target} PUBLIC    ${in_LINK_PUBLIC})
      target_link_libraries(${in_target} PRIVATE   ${in_LINK_PRIVATE})
      target_link_libraries(${in_target} INTERFACE ${in_LINK_INTERFACE})
      target_link_libraries(${in_target} PRIVATE   ${CMAKE_DL_LIBS})
    endif()

  endif()

endfunction()
