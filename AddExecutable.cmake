# ┌──────────────────────────────────────────────────────────────────┐
# │ ViDi Codebase                                                    │
# │  Copyright(c) 2018 - 2020 Qi Wu                                  │
# │  University of California, Davis                                 │
# │  MIT Licensed                                                    │
# └──────────────────────────────────────────────────────────────────┘
include_guard(GLOBAL)
include(${CMAKE_CURRENT_LIST_DIR}/CMakeProperty.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/TargetLinkage.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/TargetDefine.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/TargetDefault.cmake)

# ┌────────────────────────────────────────────────────────────────────┐
# │ API: helper function to create an application                      │
# └────────────────────────────────────────────────────────────────────┘
function(vidi_add_executable
  in_target)

  # parse arguments
  set(options)
  set(oneArgs)
  set(mulArgs
    # from vidi_target_link
    LINK LINK_PUBLIC LINK_PRIVATE LINK_INTERFACE
    # from vidi_target_configure
    DEFINE_PUBLIC DEFINE_PRIVATE DEFINE_INTERFACE
    # others
    SOURCES HEADERS OBJECTS)
  cmake_parse_arguments(in "${options}" "${oneArgs}" "${mulArgs}" ${ARGN})
  if(in_UNPARSED_ARGUMENTS)
    foreach(arg ${in_UNPARSED_ARGUMENTS})
      message(WARNING "Unparsed argument: ${arg}")
    endforeach()
  endif()

  # create Application
  foreach(obj ${in_OBJECTS})
    list(APPEND in_SOURCES $<TARGET_OBJECTS:${obj}>)
  endforeach()
  add_executable(${in_target} ${in_SOURCES} ${in_HEADERS})

  vidi_target_define(${in_target}
    DEFINE_PUBLIC ${in_DEFINE_PUBLIC}
    DEFINE_PRIVATE ${in_DEFINE_PRIVATE}
    DEFINE_INTERFACE ${in_DEFINE_INTERFACE})

  vidi_target_linkage(${in_target}
    LINK ${in_LINK}
    LINK_PUBLIC ${in_LINK_PUBLIC}
    LINK_PRIVATE ${in_LINK_PRIVATE}
    LINK_INTERFACE ${in_LINK_INTERFACE}
    LINK_OBJECT ${in_OBJECTS})

endfunction()
