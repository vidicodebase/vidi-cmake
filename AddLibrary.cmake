# ┌──────────────────────────────────────────────────────────────────┐
# │ ViDi Codebase                                                    │
# │  Copyright(c) 2018 - 2020 Qi Wu                                  │
# │  University of California, Davis                                 │
# │  MIT Licensed                                                    │
# └──────────────────────────────────────────────────────────────────┘
include_guard(GLOBAL)
include(${CMAKE_CURRENT_LIST_DIR}/CMakeProperty.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/TargetLinkage.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/TargetDefine.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/TargetDefault.cmake)

# ┌────────────────────────────────────────────────────────────────────┐
# │ API: helper function to create a library                           │
# └────────────────────────────────────────────────────────────────────┘
function(vidi_add_library
  in_target)

  # parse arguments
  set(options)
  set(oneArgs
    NAMESPACE
    OUTPUT
    EXPORT
    INSTALL)
  set(mulArgs
    # target linkages
    LINK LINK_PUBLIC LINK_PRIVATE LINK_INTERFACE
    # target definitions
    DEFINE_PUBLIC DEFINE_PRIVATE DEFINE_INTERFACE
    SOURCES  # source files
    HEADERS  # header files
    OBJECTS) # object libraries
  cmake_parse_arguments(in "${options}" "${oneArgs}" "${mulArgs}" ${ARGN})
  if(in_UNPARSED_ARGUMENTS)
    foreach(arg ${in_UNPARSED_ARGUMENTS})
      message(WARNING "Unparsed argument: ${arg}")
    endforeach()
  endif()

  # create Libraries
  foreach(obj ${in_OBJECTS})
    list(APPEND in_SOURCES $<TARGET_OBJECTS:${obj}>)
  endforeach()
  add_library(${in_target} SHARED ${in_SOURCES} ${in_HEADERS})

  # add namespace if necessary
  if(in_NAMESPACE)
    add_library(${in_NAMESPACE}::${in_target} ALIAS ${in_target})
  endif()

  # (windows) create dll export header
  if(in_NAMESPACE)
    set(dllexport_header ${in_NAMESPACE}_${in_target})
    generate_export_header(${in_target} BASE_NAME ${in_NAMESPACE}_${in_target})
  else()
    set(dllexport_header ${in_target})
    generate_export_header(${in_target})
  endif()

  # library properties
  if(in_OUTPUT)
    set_property(TARGET ${in_target} PROPERTY OUTPUT_NAME ${in_OUTPUT})
  elseif(in_NAMESPACE)
    set_property(TARGET ${in_target} PROPERTY OUTPUT_NAME ${in_NAMESPACE}_${in_target})
  endif()

  # other properties
  vidi_target_property_set_header(
    ${in_target} ${in_HEADERS}
  )
  vidi_target_property_set_export(
    ${in_target} ${in_EXPORT}
  )
  vidi_target_property_set_install(
    ${in_target} ${in_INSTALL}
  )
  vidi_target_property_set_namespace(
    ${in_target} ${in_NAMESPACE}
  )
  vidi_target_property_set_dllexport(
    ${in_target} ${dllexport_header}
  )

  vidi_target_define(${in_target}
    DEFINE_PUBLIC ${in_DEFINE_PUBLIC}
    DEFINE_PRIVATE ${in_DEFINE_PRIVATE}
    DEFINE_INTERFACE ${in_DEFINE_INTERFACE})

  vidi_target_linkage(${in_target}
    LINK ${in_LINK}
    LINK_PUBLIC ${in_LINK_PUBLIC}
    LINK_PRIVATE ${in_LINK_PRIVATE}
    LINK_INTERFACE ${in_LINK_INTERFACE}
    LINK_OBJECT ${in_OBJECTS})

endfunction()
