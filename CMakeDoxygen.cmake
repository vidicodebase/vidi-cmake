# ┌──────────────────────────────────────────────────────────────────┐
# │  Build Documentation                                             │
# └──────────────────────────────────────────────────────────────────┘
include_guard(GLOBAL)
find_package(Doxygen)
find_package(Perl)

function(vidi_doc_add)
  foreach(f ${ARGN})
    set_property(GLOBAL APPEND PROPERTY __vidi__DOC_SOURCE
      ${CMAKE_CURRENT_LIST_DIR}/${f})
  endforeach()
endfunction()

file(WRITE ${CMAKE_BINARY_DIR}/_doc.h
  "#pragma once\n"
  "namespace std {\n"
  "  /**\n"
  "    * @brief This is a dummy function created for doxygen to recognize std::shared_ptr. \n"
  "    * This file will not be included in any actual code */\n"
  "  template<class T> class shared_ptr { T *dummy; };\n"
  "  /**\n"
  "    * @brief This is a dummy function created for doxygen to recognize std::unique_ptr. \n"
  "    * This file will not be included in any actual code */\n"
  "  template<class T> class unique_ptr { T *dummy; };\n"
  "}\n")

function(vidi_doc_gen)
  if(DOXYGEN_FOUND AND PERL_FOUND)
    get_property(doc_files GLOBAL PROPERTY __vidi__DOC_SOURCE)
    doxygen_add_docs(document ${doc_files}
      ${CMAKE_BINARY_DIR}/_doc.h
      COMMENT "Generate man pages")
  else()
    message(WARNING "Doxygen is required for building the documentation")
  endif()
endfunction()
