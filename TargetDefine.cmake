# ┌──────────────────────────────────────────────────────────────────┐
# │ ViDi Codebase                                                    │
# │  Copyright(c) 2018 - 2020 Qi Wu                                  │
# │  University of California, Davis                                 │
# │  MIT Licensed                                                    │
# └──────────────────────────────────────────────────────────────────┘
include_guard(GLOBAL)

# ┌────────────────────────────────────────────────────────────────────┐
# │ API: This step adds compile definitions to the target              |
# └────────────────────────────────────────────────────────────────────┘
function(vidi_target_define
  in_target)

  # parse arguments
  set(options)
  set(oneValueArgs)
  set(multiValueArgs
    DEFINE_PUBLIC
    DEFINE_PRIVATE
    DEFINE_INTERFACE)
  cmake_parse_arguments(in
    "${options}"
    "${oneValueArgs}"
    "${multiValueArgs}"
    ${ARGN})
  if(in_UNPARSED_ARGUMENTS)
    foreach(arg ${in_UNPARSED_ARGUMENTS})
      message(WARNING "Unparsed argument: ${arg}")
    endforeach()
  endif()

  # compile definitions
  if(in_DEFINE_PUBLIC)
    target_compile_definitions(${in_target}
      PUBLIC ${in_DEFINE_PUBLIC})
  endif()
  if(in_DEFINE_PRIVATE)
    target_compile_definitions(${in_target}
      PRIVATE ${in_DEFINE_PRIVATE})
  endif()
  if(in_DEFINE_INTERFACE)
    target_compile_definitions(${in_target}
      INTERFACE ${in_DEFINE_INTERFACE})
  endif()

endfunction()
